create table locations
(
    location_id    serial      not null
        constraint locations_pkey
            primary key,
    street_address varchar(40),
    city           varchar(30) not null
);

alter table locations
    owner to postgres;

create table departments
(
    department_id   serial      not null
        constraint departments_pkey
            primary key,
    department_name varchar(30) not null,
    manager_id      integer,
    location_id     integer
        constraint departments_location_id_fkey
            references locations
);

alter table departments
    owner to postgres;

create table jobs
(
    -- Only integer types can be auto increment
    job_id     varchar default nextval('jobs_job_id_seq'::regclass) not null
        constraint job_pkey
            primary key,
    job_title  varchar(35)                                          not null,
    min_salary integer,
    max_salary integer
);

alter table jobs
    owner to postgres;

create table employees
(
    employee_id   serial      not null
        constraint employees_pkey
            primary key,
    first_name    varchar(20),
    last_name     varchar(25) not null,
    email         varchar(25) not null,
    phone_number  varchar(20),
    hire_date     date        not null,
    job_id        varchar(10) not null
        constraint employees_job_id_fkey
            references jobs,
    salary        integer,
    manager_id    integer
        constraint employees_manager_id_fkey
            references employees,
    department_id integer
        constraint employees_department_id_fkey
            references departments
);

alter table employees
    owner to postgres;

alter table departments
    add constraint departments_manager_id_fkey
        foreign key (manager_id) references employees;

create table job_history
(
    start_date    date        not null,
    end_date      date        not null,
    job_id        varchar(10) not null
        constraint job_history_job_id_fkey
            references jobs,
    department_id integer
        constraint job_history_department_id_fkey
            references departments,
    employee_id   integer     not null
        constraint job_history_employee_id_fkey
            references employees,
    constraint job_history_pkey
        primary key (start_date, employee_id)
);

alter table job_history
    owner to postgres;

create unique index employees_email_uindex
    on employees (email);
