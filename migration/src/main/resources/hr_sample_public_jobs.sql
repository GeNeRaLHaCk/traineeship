INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('1', 'Программист', 50000, 200000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('2', 'Архитектор бизнес-процессов', 70000, 300000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('3', 'Администратор баз данных', 60000, 150000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('4', 'Администратор защиты', 80000, 180000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('5', 'Аналитик программного обеспечения', 65000, 190000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('6', 'Архитектор программного обеспечения', 90000, 250000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('7', 'Таргетолог', 50000, 170000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('8', 'HR-менеджер', 50000, 130000);