1)SELECT count(*) AS rows from locations;
select count(Distinct street_address) as "distinct_name" from locations;
select count(*) - count(distinct street_address) as "duplicate_names" from locations;
select count(distinct street_address) / count(*) as unique,
       1-(count(distinct street_address) / count(*)) as ununique
From locations;
2)SELECT max(employees.salary), department_name from employees INNER JOIN
                                                     departments d on d.department_id = employees.department_id
  Group By department_name;
3)
SELECT employees.employee_id, employees.first_name, employees.last_name,
       employees.email, employees.phone_number, employees.hire_date,
       j.job_title, d.department_name, emp.last_name as "manager"  from employees
inner join departments d on d.department_id = employees.department_id
inner join employees emp on employees.manager_id = emp.employee_id
inner join jobs j on employees.job_id = j.job_id
4)//Получаем уникальные значения
SELECT street_address, city from locations group by street_address, city
//удаляем повторяющиеся
delete from locations
WHERE street_address IN (
    SELECT
        street_address
    FROM (
             SELECT
                 street_address,
                 row_number() OVER w as rnum
             FROM locations
                      WINDOW w AS (
                     PARTITION BY street_address
                     ORDER BY location_id
                     )
         ) t
    WHERE t.rnum > 1);
5)//делим таблицу на равные части и выбираем какую часть показать
//(ntile(столько, на сколько частей необходимо разделить))
select *
from (select employee_id, ntile(2) over(order by employee_id) nt from employees) t
where nt = 1;
6)//сотрудники, которые не прикреплены ни к какому отделу
select * from employees
where department_id is null;
7)//представление с сотрудниками
create view EmployeesVIEW AS SELECT employees.employee_id, employees.first_name, employees.last_name,
                                                 employees.email, employees.phone_number, employees.hire_date,
                                                 j.job_title, d.department_name, emp.last_name as "manager"  from employees
inner join departments d on d.department_id = employees.department_id
inner join employees emp on employees.manager_id = emp.employee_id
inner join jobs j on employees.job_id = j.job_id;
8)