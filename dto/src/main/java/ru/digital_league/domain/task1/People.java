package ru.digital_league.domain.task1;

import java.util.Date;

public class People {
    public People (String secondName, String firstName, int age) {
        this.FirstName = firstName;
        this.SecondName = secondName;
        this.Age = age;
    }
    private String FirstName;
    private String SecondName;
    private String MiddleName;
    private int Age;
    enum Gender { Man, Women }
    private Date dateOfBirth;
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public String getMiddleName() {
        return MiddleName;
    }

    public void setMiddleName(String middleName) {
        MiddleName = middleName;
    }


}
