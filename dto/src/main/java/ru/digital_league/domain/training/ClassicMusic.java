package ru.digital_league.domain.training;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class ClassicMusic implements Music {
    private ClassicMusic(){}
    public static ClassicMusic getClassicalMusic(){
        return new ClassicMusic();
    }
    // @PostConstruct
    public void doMyInit(){
        System.out.println("Doing my initialization");
    }
    // @PreDestroy
    public void doMyDestroy(){
        System.out.println("Doing my destroy");
    }
    @Override
    public String getSong() {
        return "Gangsta Paradise";
    }
}
