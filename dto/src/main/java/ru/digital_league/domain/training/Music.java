package ru.digital_league.domain.training;

public interface Music {
    String getSong();
}
