package ru.digital_league.domain.training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Computer {
    private int id;

    private MusicPlayer musicPlayer;
    @Autowired
    public Computer(MusicPlayer musicPlayer) {
        this.id = 1;
        this.musicPlayer = musicPlayer;
    }
    public MusicPlayer getMusicPlayer(){
        return musicPlayer;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(Music music : musicPlayer.getMusicList())
            sb.append(music.getSong() + ";");
        return "SpringConfiguration.ru.digital_league.domain.training.Computer{" +
                "id=" + id +
                ", musicPlayer play:" + sb.toString();
    }
}