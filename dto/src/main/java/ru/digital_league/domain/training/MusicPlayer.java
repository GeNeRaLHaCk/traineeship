package ru.digital_league.domain.training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MusicPlayer {

    private List<Music> musicList = new ArrayList<>();
    @Value("${musicPlayer.name}")
    private String name;
    @Value("${musicPlayer.volume}")
    private int volume;

    public MusicPlayer(){}
    @Autowired
    public MusicPlayer(@Qualifier("rockMusic") Music music1,
                       @Qualifier("classicMusic") Music music2){
        this.musicList.add(music1);
        this.musicList.add(music2);
    }
    public MusicPlayer(List<Music> musics){
        this.musicList = musics;
    }

    public void playMusic(){
        for(Music music : musicList)
            System.out.println("Playing:" + music.getSong());
    }
    public String playMusicStr(){
        return "SpringConfiguration.ru.digital_league.domain.training.Computer Playing:" + musicList.get(0).getSong();
    }

    public void setMusicList(List<Music> musics){
        this.musicList = musics;
    }

    public List<Music> getMusicList(){
        return this.musicList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }
}