
package ru.digital_league.domain.task2;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ExchangeValueRepository extends JpaRepository<ExchangeValue, Long> {
    ExchangeValue findByFromAndTo(String From, String to);
}

