package ru.digital_league.core.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.digital_league.domain.task2.ExchangeValue;
import ru.digital_league.domain.task2.ExchangeValueRepository;


@Controller
@RequestMapping("/curses")
public class ForexController {
    @Autowired
    private Environment environment;
    @Autowired
    private ExchangeValueRepository repository;

    @GetMapping("/hello-world")
    public String sayHello(){
        return "Hello-World! ";
    }
    @GetMapping("/curses")
    public String getCurses(){
        return "/curses";
    }
    @GetMapping("/currency-exchange/from/{from}/to/{to}")
    public ExchangeValue retrieveExchangeValue
            (@PathVariable String from, @PathVariable String to){
        ExchangeValue exchangeValue =
                repository.findByFromAndTo(from, to);
        exchangeValue.getFrom();
        exchangeValue.setPort(
                Integer.parseInt(environment.getProperty("local.server.port")));
        return exchangeValue;
    }
}