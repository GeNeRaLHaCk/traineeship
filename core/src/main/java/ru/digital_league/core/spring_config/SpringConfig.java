package ru.digital_league.core.spring_config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("ru.digital_league.domain")
@PropertySource("classpath:application.properties")
//@EnableWebMvc//<mvc:annotation-driven/> конфигурация полностью эквивалентна xmlMVCcontext
public class SpringConfig //implements WebMvcConfigurer этот интерфейс реализуется когда мы под себя
{
/*    //хотим настроить SpringMVC (установить шаблонизатор Thymeleaf)
    private final ApplicationContext applicationContext;
    @Autowired //ApplicationContext, благодаря тегу будет внедрён спрингом за нас сюда
    public SpringConfig(ApplicationContext applicationContext){
        this.applicationContext = applicationContext;
    }
    @Bean
    public SpringResourceTemplateResolver templateResolver(){
        SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
        templateResolver.setApplicationContext(applicationContext);
        templateResolver.setPrefix("src/main/webapp/WEB-INF/pages");
        templateResolver.setSuffix(".html");
        return templateResolver;
    }
    @Bean
    public SpringTemplateEngine templateEngine(){
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver());
        templateEngine.setEnableSpringELCompiler(true);
        return templateEngine;
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) { //Thymeleaf
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        resolver.setTemplateEngine(templateEngine());
        registry.viewResolver(resolver);
    }*/
}
